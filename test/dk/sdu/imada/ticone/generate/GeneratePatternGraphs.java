package dk.sdu.imada.ticone.generate;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Arrays;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Created by christian on 9/15/15.
 */
public class GeneratePatternGraphs {

	private double min_value;
	private double max_value;
	private XYSeriesCollection dataset;
	private double[] data;

	public GeneratePatternGraphs(double[] data, int range) {
		this.data = data;
		min_value = -range;
		max_value = range;
		setupData(data);
	}

	public void setupData(double[] data) {
		dataset = new XYSeriesCollection();
		XYSeries patternSeries = generateDataset(Arrays.toString(data), data);
		dataset.addSeries(patternSeries);
	}

	public ChartPanel createChartPanel() {
		JFreeChart xyLineChart = ChartFactory.createXYLineChart("", // Chart label/name
				"", // X-axis label
				"", // Y-axis label
				dataset, // Dataset
				PlotOrientation.VERTICAL, // Orientation
				false, // Legend
				false, // Tooltips
				false // Urls
		);

		ChartPanel chartPanel = new ChartPanel(xyLineChart);
		chartPanel.setDomainZoomable(false);
		chartPanel.setRangeZoomable(false);

		chartPanel.setPreferredSize(new Dimension(250, 100));
		XYPlot plot = xyLineChart.getXYPlot();
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

		// Set tick to go 1 up at a time
		NumberAxis numberAxis = new NumberAxis();
		numberAxis.setTickUnit(new NumberTickUnit(1));
		numberAxis.setRange(1, data.length);
		plot.setDomainAxis(numberAxis);

		// Set y-axis to go from -3 to 3
		int range_min = (int) min_value;
		int range_max = (int) max_value;
		ValueAxis valueAxis = plot.getRangeAxis();

		valueAxis.setRange(range_min, range_max);
		plot.setRangeAxis(valueAxis);

		// Line color of actual pattern
		renderer.setSeriesPaint(0, Color.black);

		// Do not show points for pattern
		renderer.setSeriesShapesVisible(0, false);

		// Line width of actual pattern
		renderer.setSeriesStroke(0, new BasicStroke(3f));

		plot.setRenderer(renderer);
		return chartPanel;
	}

	private XYSeries generateDataset(String name, double[] data) {
		XYSeries xySeries = new XYSeries(name);
		for (int i = 0; i < data.length; i++) {
			if (data[i] > max_value) {
				max_value = data[i];
			}
			if (data[i] < min_value) {
				min_value = data[i];
			}
			xySeries.add(i + 1, data[i]);
		}
		return xySeries;
	}
}
