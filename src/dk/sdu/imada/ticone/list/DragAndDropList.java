package dk.sdu.imada.ticone.list;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;

public class DragAndDropList<E> extends JList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4339198789309864772L;

	public DragAndDropList(final ListModel<E> model) {
		super(model);
		this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		this.setTransferHandler(new ListItemTransferHandler());
		this.setDropMode(DropMode.INSERT);
		this.setDragEnabled(true);
	}
}

// @camickr already suggested above.
// http://docs.oracle.com/javase/tutorial/uiswing/dnd/dropmodedemo.html
class ListItemTransferHandler extends TransferHandler {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6882912270264922919L;
	private final DataFlavor localObjectFlavor;
	private Object[] transferedObjects = null;

	public ListItemTransferHandler() {
		this.localObjectFlavor = new ActivationDataFlavor(Object[].class, DataFlavor.javaJVMLocalObjectMimeType,
				"Array of items");
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Transferable createTransferable(final JComponent c) {
		final JList list = (JList) c;
		this.indices = list.getSelectedIndices();
		this.transferedObjects = list.getSelectedValues();
		return new DataHandler(this.transferedObjects, this.localObjectFlavor.getMimeType());
	}

	@Override
	public boolean canImport(final TransferSupport info) {
		if (!info.isDrop() || !info.isDataFlavorSupported(this.localObjectFlavor)) {
			return false;
		}
		return true;
	}

	@Override
	public int getSourceActions(final JComponent c) {
		return MOVE; // TransferHandler.COPY_OR_MOVE;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean importData(final TransferSupport info) {
		if (!this.canImport(info)) {
			return false;
		}
		final JList target = (JList) info.getComponent();
		final JList.DropLocation dl = (JList.DropLocation) info.getDropLocation();
		final DefaultListModel listModel = (DefaultListModel) target.getModel();
		int index = dl.getIndex();
		final int max = listModel.getSize();
		if (index < 0 || index > max) {
			index = max;
		}
		this.addIndex = index;
		try {
			final Object[] values = (Object[]) info.getTransferable().getTransferData(this.localObjectFlavor);
			this.addCount = values.length;
			for (int i = 0; i < values.length; i++) {
				final int idx = index++;
				listModel.add(idx, values[i]);
				target.addSelectionInterval(idx, idx);
			}
			return true;
		} catch (final UnsupportedFlavorException ufe) {
			ufe.printStackTrace();
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		}
		return false;
	}

	@Override
	protected void exportDone(final JComponent c, final Transferable data, final int action) {
		this.cleanup(c, action == MOVE);
	}

	private void cleanup(final JComponent c, final boolean remove) {
		if (remove && this.indices != null) {
			final JList source = (JList) c;
			final DefaultListModel model = (DefaultListModel) source.getModel();
			if (this.addCount > 0) {
				// http://java-swing-tips.googlecode.com/svn/trunk/DnDReorderList/src/java/example/MainPanel.java
				for (int i = 0; i < this.indices.length; i++) {
					if (this.indices[i] >= this.addIndex) {
						this.indices[i] += this.addCount;
					}
				}
			}
			for (int i = this.indices.length - 1; i >= 0; i--) {
				model.remove(this.indices[i]);
			}
		}
		this.indices = null;
		this.addCount = 0;
		this.addIndex = -1;
	}

	private int[] indices = null;
	private int addIndex = -1; // Location where items were added
	private int addCount = 0; // Number of items added.
}
