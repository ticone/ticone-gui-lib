package dk.sdu.imada.ticone.util;

import java.awt.Color;
import java.awt.LinearGradientPaint;
import java.awt.geom.Point2D;

import org.apache.commons.lang3.tuple.Pair;

import dk.sdu.imada.ticone.clustering.ICluster;

/**
 * Created by christian on 6/3/15.
 */

public class ColorUtility {
	private static final String[] indexcolors = new String[] { "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941",
			"#006FA6", "#A30059", "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF",
			"#997D87", "#5A0007", "#809693", /* "#FEFFE6", */ "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
			"#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100", "#DDEFFF",
			"#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F", "#372101", "#FFB500",
			"#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09", "#00489C", "#6F0062", "#0CBD66",
			"#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66", "#885578", "#FAD09F", "#FF8A9A", "#D157A0",
			"#BEC459", "#456648", "#0086ED", "#886F4C", "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375",
			"#A3C8C9", "#FF913F", "#938A81", "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757",
			"#C8A1A1", "#1E6E00", "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF",
			"#9B9700", "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
			"#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C", "#83AB58",
			"#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800", "#BF5650", "#E83000",
			"#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51", "#C895C5", "#320033", "#FF6832",
			"#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58" };

	public Color c = new Color(180, 168, 189 /* B4A8BD */);

	public static String getColor(final ICluster c) {
		final int j = (int) (c.getInternalClusterId() % indexcolors.length);
		return indexcolors[j];
	}

	public static Color calculateComplementColor(final Color color) {
		final int red = color.getRed();
		final int green = color.getGreen();
		final int blue = color.getBlue();
		final float[] hsb = Color.RGBtoHSB(red, green, blue, null);
		hsb[0] = (hsb[0] + 0.5f) % 1;

		final Color complementColor = new Color(Color.HSBtoRGB(hsb[0], hsb[1], hsb[2]));

		return complementColor;
	}

	public static double calculateLuminance(final Color color) {
		final double r = color.getRed();
		final double g = color.getGreen();
		final double b = color.getBlue();
		final double[] c = new double[] { r, g, b };
		for (int i = 0; i < 3; i++) {
			c[i] /= 255.0;
			if (c[i] <= 0.03928)
				c[i] /= 12.92;
			else
				c[i] = Math.pow(((c[i] + 0.055) / 1.055), 2.4);
		}
		final double l = 0.2126 * c[0] + 0.7152 * c[1] + 0.0722 * c[2];
		return l;
	}

	public static Color calculateTextColor(final Color color) {
		// if (r * 0.299 + g * 0.587 + b * 0.114 > 186) {
		// return Color.black;
		// } else {
		// return Color.white;
		// }
		final double l = calculateLuminance(color);
		if (l > Math.sqrt(1.05 * 0.05) - 0.05) {
			return Color.BLACK;
		}
		return Color.WHITE;
	}

	public static Color calculateCommonTextColor(final Pair<Color, Color> colorPair) {
		final double l1 = calculateLuminance(colorPair.getLeft());
		final double l2 = calculateLuminance(colorPair.getRight());

		final double l = l1 > l2 ? l1 : l2;
		if (l > Math.sqrt(1.05 * 0.05) - 0.05) {
			return Color.black;
		} else {
			return Color.white;
		}
	}

	public static Color calculateTextColor(final Pair<Color, Color> colorPair) {
		final double l = calculateLuminance(colorPair.getLeft());
		if (l > Math.sqrt(1.05 * 0.05) - 0.05) {
			return Color.black;
		} else {
			return Color.white;
		}
	}

	public static LinearGradientPaint calculateTextGradient(final Pair<Color, Color> colorPair) {
		final Color color1 = calculateTextColor(colorPair.getLeft()), color2 = calculateTextColor(colorPair.getRight());

		return new LinearGradientPaint(new Point2D.Float(0.0f, 0.0f), new Point2D.Float(1.0f, 0.0f),
				new float[] { 0.0f, 0.475f, 0.5f, 0.525f, 1.0f },
				new Color[] { color1, color1, Color.BLACK, color2, color2 });
	}
}
