/**
 * 
 */
package dk.sdu.imada.ticone.gui;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

import javax.swing.JLabel;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.FeatureValuesStoredEvent;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.feature.store.IFeatureValueStoredListener;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 20, 2019
 *
 */
public class FeatureValueLabel<V> extends JLabel implements IFeatureValueStoredListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7929412263952250154L;

	private IFeatureStore featureStore;
	private final IFeature<V> feature;
	private final IObjectWithFeatures object;
	private final Function<V, String> valueToString;

	public FeatureValueLabel(IFeatureStore featureStore, IFeature<V> feature, IObjectWithFeatures object,
			Function<V, String> valueToString) {
		super();
		this.featureStore = featureStore;
		this.feature = feature;
		this.object = object;
		this.valueToString = valueToString;

		this.featureStore.addFeatureValueStoredListener(this);

		updateText();
	}

	/**
	 * @param featureStore the featureStore to set
	 */
	public void setFeatureStore(IFeatureStore featureStore) {
		if (featureStore == this.featureStore)
			return;

		if (this.featureStore != null)
			this.featureStore.removeFeatureValueStoredListener(this);
		this.featureStore = featureStore;
		if (this.featureStore != null)
			this.featureStore.addFeatureValueStoredListener(this);

		this.updateText();
	}

	/**
	 * @return the featureStore
	 */
	public IFeatureStore getFeatureStore() {
		return this.featureStore;
	}

	@Override
	public void featureValuesStored(FeatureValuesStoredEvent e) {
		if (e.getStore() != this.featureStore)
			return;

		Map<IFeature<?>, Collection<IObjectWithFeatures>> objectFeaturePairs = e.getObjectFeaturePairs();
		if (objectFeaturePairs.containsKey(feature) && objectFeaturePairs.get(feature).contains(object))
			updateText();
	}

	private void updateText() {
		String valueText;
		if (this.featureStore.hasFeatureFor(feature, object))
			valueText = valueToString.apply(this.featureStore.getFeatureValue(object, feature).getValue());
		else
			valueText = "--";
		this.setText(valueText);
	}

}
