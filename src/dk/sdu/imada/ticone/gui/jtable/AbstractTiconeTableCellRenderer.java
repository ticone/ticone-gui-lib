/**
 * 
 */
package dk.sdu.imada.ticone.gui.jtable;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.CellEditorListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeaturesProxy;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public abstract class AbstractTiconeTableCellRenderer extends DefaultTableCellRenderer implements TableCellEditor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6634010295569421868L;
	protected Collection<? extends IObjectWithFeatures> newObjects;
	protected Collection<? extends IObjectWithFeatures> deletedObjects;
	protected Collection<? extends IObjectWithFeatures> filteredObjects;
	protected Color deletedColor = new Color(1f, 0.7f, 0.7f);
	protected Color newColor = new Color(0.7f, 1f, 0.7f);
	protected Color filteredFontColor = Color.GRAY;
	protected Color filteredBGColor = Color.LIGHT_GRAY;
	protected final Color backgroundColor = this.getBackground();

	/**
	 * 
	 */
	public AbstractTiconeTableCellRenderer() {
		super();
		this.newObjects = new ArrayList<>();
		this.deletedObjects = new ArrayList<>();
		this.filteredObjects = new ArrayList<>();
		this.setHorizontalAlignment(CENTER);
		this.setHorizontalTextPosition(CENTER);
		this.setVerticalAlignment(CENTER);
		this.setVerticalTextPosition(CENTER);
	}

	public void setDeletedObjects(final Collection<? extends IObjectWithFeatures> deletedObjects) {
		this.deletedObjects = deletedObjects;
	}

	public void setNewObjects(final Collection<? extends IObjectWithFeatures> newObjects) {
		this.newObjects = newObjects;
	}

	public void setFilteredObjects(final Collection<? extends IObjectWithFeatures> objects) {
		this.filteredObjects = objects;
	}

	protected void setColors(final JTable table, final IObjectWithFeatures object, final boolean isSelected,
			final boolean hasFocus, final int row, final int column, final Component c) {
		if (isSelected) {
			c.setBackground(table.getSelectionBackground());
			c.setForeground(table.getSelectionForeground());
		} else if (this.newObjects.contains(object)) {
			c.setBackground(this.newColor);
		} else if (this.deletedObjects.contains(object)) {
			c.setBackground(this.deletedColor);
		} else if (this.filteredObjects.contains(object)) {
			c.setBackground(this.filteredBGColor);
			c.setForeground(this.filteredFontColor);
		} else {
			final Color col = row % 2 == 0 ? Color.WHITE
					: UIManager.getLookAndFeelDefaults().contains("Table.alternateRowColor")
							? (Color) UIManager.getLookAndFeelDefaults().get("Table.alternateRowColor")
							: new Color(240, 240, 240);
			c.setBackground(col);
			c.setForeground(table.getForeground());
		}
	}

	/**
	 * The returned object is used to identify {@link #newObjects},
	 * {@link #deletedObjects}, {@link #filteredObjects} and set appropriate colors
	 * in
	 * {@link #setColors(JTable, IObjectWithFeatures, boolean, boolean, int, int, Component)}.
	 * 
	 * @param table
	 * @param row
	 * @param column
	 * @param value
	 * @return
	 */
	protected IObjectWithFeatures getObjectFromValue(final JTable table, final int row, final int column,
			final Object value) {
		if (value instanceof IObjectWithFeaturesProxy)
			return ((IObjectWithFeaturesProxy) value).getObjectWithFeatures();
		else if (value instanceof IObjectWithFeatures)
			return (IObjectWithFeatures) value;
		return null;
	}

	@Override
	public void addCellEditorListener(final CellEditorListener cellEditorListener) {

	}

	@Override
	public void cancelCellEditing() {

	}

	@Override
	public Object getCellEditorValue() {
		return null;
	}

	@Override
	public Component getTableCellEditorComponent(final JTable jTable, final Object value, final boolean b,
			final int row, final int column) {
		return new JLabel(((IObjectWithFeatures) value).getName());
	}

	@Override
	public boolean isCellEditable(final EventObject eventObject) {
		return false;
	}

	@Override
	public void removeCellEditorListener(final CellEditorListener cellEditorListener) {

	}

	@Override
	public boolean stopCellEditing() {
		return true;
	}

	@Override
	public boolean shouldSelectCell(final EventObject eventObject) {
		return true;
	}
}