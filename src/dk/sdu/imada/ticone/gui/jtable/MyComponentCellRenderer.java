package dk.sdu.imada.ticone.gui.jtable;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.JLabel;
import javax.swing.JTable;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

/**
 * @author Christian Wiwie
 * @author Christian Norskov
 * @since 9/4/15
 */
public class MyComponentCellRenderer<OBJ extends IObjectWithFeatures> extends AbstractTiconeTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7571771818045819685L;

	@Override
	public Component getTableCellRendererComponent(final JTable jTable, final Object value, final boolean isSelected,
			final boolean hasFocus, final int row, final int column) {
		final Component c = (Component) value;

		final IObjectWithFeatures object = getObjectFromValue(jTable, row, column, value);
		if (object == null)
			return new JLabel("ERROR");

		this.setColors(jTable, object, isSelected, hasFocus, row, column, c);

		return c;
	}

	@Override
	public Component getTableCellEditorComponent(JTable jTable, Object value, boolean b, int row, int column) {
		final Component c = (Component) value;

		final IObjectWithFeatures object = getObjectFromValue(jTable, row, column, value);
		if (object == null)
			return new JLabel("ERROR");

		this.setColors(jTable, object, false, false, row, column, c);

		return c;
	}

	@Override
	public boolean isCellEditable(EventObject eventObject) {
		return true;
	}
}
