package dk.sdu.imada.ticone.gui.panels.clusterchart;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.TiconeVisualClusteringResult;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.JFreeChartBuilder;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.ColorUtility;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;

public class ClusterChartContainer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6649740571032532374L;
	public static final int MAXIMAL_NUMBER_TIME_SERIES_PER_CHART = 1000;
	private final ITimeSeriesObjects allObjects;
	private final ICluster cluster;
	private final Double[][] percentiles;
	private Color graphColor;
	private final List<List<Double>> allTimePoints;
	private final Map<ITimeSeriesObject, Integer> objectsDatasetIndex;
	private int timePoints;
	private int numberVisualizedObjects;

	private CHART_Y_LIMITS_TYPE chartLimits;
	private JFreeChartBuilder chartFactory;

	public enum CHART_Y_LIMITS_TYPE {
		MIN_MAX, FIVE_QUANTILE, TWENTYFIVE_QUANTILE
	}

	public ClusterChartContainer(final TiconeVisualClusteringResult utils, final ICluster p,
			final ITimeSeriesObjects allObjects)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
		this(p, allObjects, utils.getChartLimits(), utils.getChartFactory());
	}

	public ClusterChartContainer(final ICluster p, final ITimeSeriesObjects allObjects,
			final CHART_Y_LIMITS_TYPE chartLimits, final JFreeChartBuilder chartFactory)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
		super();
		this.chartLimits = chartLimits;
		this.chartFactory = chartFactory;

		this.allObjects = allObjects;
		this.cluster = p;

		this.objectsDatasetIndex = new HashMap<>();

		this.allTimePoints = new ArrayList<>();

		if (allObjects.size() > 0) {
			this.timePoints = allObjects.iterator().next().getPreprocessedTimeSeriesList()[0].getNumberTimePoints();
			this.numberVisualizedObjects = MAXIMAL_NUMBER_TIME_SERIES_PER_CHART
					/ allObjects.iterator().next().getSampleNameList().size();
			this.numberVisualizedObjects = Math.min(allObjects.size(), this.numberVisualizedObjects);
		} else if (this.cluster != null && this.cluster.getPrototype() != null) {
			this.timePoints = PrototypeComponentType.TIME_SERIES.getComponent(this.cluster.getPrototype())
					.getTimeSeries().getNumberTimePoints();
			this.numberVisualizedObjects = 0;
		} else {
			this.timePoints = 1;
		}

		for (int i = 0; i < this.timePoints; i++) {
			this.allTimePoints.add(new ArrayList<Double>());
		}

		this.percentiles = new Double[this.timePoints][6];

		int alphaValue = 255;
		if (allObjects.size() > 0) {
			alphaValue = Math.min(255, (254 / (this.numberVisualizedObjects + 1)) * 8);
		}
		alphaValue = Math.max(10, alphaValue);
		if (this.cluster == null) {
			final Color gColor = Color.blue;
			this.graphColor = new Color(gColor.getRed(), gColor.getGreen(), gColor.getBlue(), alphaValue);
		} else {
			final Color gColor = Color.decode(ColorUtility.getColor(this.cluster));
			this.graphColor = new Color(gColor.getRed(), gColor.getGreen(), gColor.getBlue(), alphaValue);
		}

	}

	public int getObjectDatasetIndex(final ITimeSeriesObject timeSeriesData) {
		if (this.objectsDatasetIndex.containsKey(timeSeriesData)) {
			return this.objectsDatasetIndex.get(timeSeriesData);
		}
		return -1;
	}

	public ChartPanel getChartPanel()
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		return this.getChartPanel(250, 100);
	}

	private JFreeChartBuilder getFreeChartFactory() {
		return this.chartFactory;
	}

	/**
	 * @return the cluster
	 */
	public ICluster getCluster() {
		return this.cluster;
	}

	public ChartPanel getChartPanel(final int panelWidth, final int panelHeight)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		return this.getChartPanel(panelWidth, panelHeight, false);
	}

	public ChartPanel getChartPanel(final int preferredPanelWidth, final int prefferedPanelHeight,
			final boolean hideAxes)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		final JFreeChartBuilder freeChartFactory = this.getFreeChartFactory();
		freeChartFactory.setLargeChart(false);
		freeChartFactory.setLimitsType(this.chartLimits);

		freeChartFactory.setAllObjects(this.allObjects);
		freeChartFactory.setAllTimePoints(this.allTimePoints);
		freeChartFactory.setCluster(this.cluster);
		freeChartFactory.setGraphColor(this.graphColor);
		freeChartFactory.setNumberVisualizedObjects(this.numberVisualizedObjects);
		freeChartFactory.setObjectsDatasetIndex(this.objectsDatasetIndex);
		freeChartFactory.setPercentiles(this.percentiles);
		freeChartFactory.setTimePoints(this.timePoints);
		freeChartFactory.setHideAxes(hideAxes);

		try {
			JFreeChart jfreeChart = freeChartFactory.build();

			final ChartPanel chartPanel = new ChartPanel(jfreeChart, preferredPanelWidth, prefferedPanelHeight,
					ChartPanel.DEFAULT_MINIMUM_DRAW_WIDTH, ChartPanel.DEFAULT_MINIMUM_DRAW_HEIGHT,
					ChartPanel.DEFAULT_MAXIMUM_DRAW_WIDTH, ChartPanel.DEFAULT_MAXIMUM_DRAW_HEIGHT,
					ChartPanel.DEFAULT_BUFFER_USED, true, true, true, true, true);
			chartPanel.setDomainZoomable(false);
			chartPanel.setRangeZoomable(true);

			chartPanel.setMinimumSize(new Dimension(preferredPanelWidth, prefferedPanelHeight));
			return chartPanel;
		} catch (final FactoryException | CreateInstanceFactoryException e) {
			if (e.getCause() instanceof IncompatiblePrototypeComponentException)
				throw (IncompatiblePrototypeComponentException) e.getCause();
			if (e.getCause() instanceof MissingPrototypeException)
				throw (MissingPrototypeException) e.getCause();
			// TODO
			e.printStackTrace();
			return null;
		}
	}

	public static File getChartAsPNGFile(final JFreeChart chart) {
		try {
			final File f = File.createTempFile("cluster", ".png");
			writeChartAsPNG(chart, f);
			return f;
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void writeChartAsPNG(final JFreeChart xyLineChart, final File f) {
		try {
			final FileOutputStream fos = new FileOutputStream(f);

			ChartUtils.writeChartAsPNG(fos, xyLineChart, 250, 150);
			fos.close();

			// clear the old temporary file
			// File old = utils.getChartPngFiles().get(cluster) != null ? new
			// File(utils.getChartPngFiles().get(cluster))
			// : null;
			// utils.setChartPngFile(cluster, f.getAbsolutePath());
			// if (old != null)
			// old.delete();
		} catch (final IOException e) {
			e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	public ChartPanel getLargeChartPanel()
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		final JFreeChartBuilder freeChartFactory = this.getFreeChartFactory();
		freeChartFactory.setLargeChart(true);
		freeChartFactory.setLimitsType(this.chartLimits);

		freeChartFactory.setAllObjects(this.allObjects);
		freeChartFactory.setAllTimePoints(this.allTimePoints);
		freeChartFactory.setCluster(this.cluster);
		freeChartFactory.setGraphColor(this.graphColor);
		freeChartFactory.setNumberVisualizedObjects(this.numberVisualizedObjects);
		freeChartFactory.setObjectsDatasetIndex(this.objectsDatasetIndex);
		freeChartFactory.setPercentiles(this.percentiles);
		freeChartFactory.setTimePoints(this.timePoints);

		JFreeChart largeJFreeChart;
		try {
			largeJFreeChart = freeChartFactory.build();
		} catch (final FactoryException | CreateInstanceFactoryException e) {
			if (e.getCause() instanceof IncompatiblePrototypeComponentException)
				throw (IncompatiblePrototypeComponentException) e.getCause();
			if (e.getCause() instanceof MissingPrototypeException)
				throw (MissingPrototypeException) e.getCause();
			// TODO
			e.printStackTrace();
			return null;
		}

		final ChartPanel chartPanel = new ChartPanel(largeJFreeChart);
		chartPanel.setDomainZoomable(false);
		chartPanel.setRangeZoomable(true);

		chartPanel.setPreferredSize(new Dimension(750, 300));
		return chartPanel;
	}

	public Color getGraphColor() {
		return this.graphColor;
	}
}
