/**
 * 
 */
package dk.sdu.imada.ticone.gui.panels.valuesample;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.List;
import java.util.Set;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.FeatureValuesStoredEvent;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.feature.store.IFeatureValueStoredListener;
import dk.sdu.imada.ticone.feature.store.INewFeatureStoreListener;
import dk.sdu.imada.ticone.feature.store.NewFeatureStoreEvent;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 4, 2019
 *
 * @param <O>
 */
public class FeatureValueHistoryScatterChartPanel<O extends IObjectWithFeatures> extends JPanel
		implements INewFeatureStoreListener, IFeatureValueStoredListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7434885749735593447L;
	protected final IFeature<? extends Number> feature;
	private final TiconeClusteringResult clusteringResult;
	protected final ObjectType<O> objectType;

	public FeatureValueHistoryScatterChartPanel(final ObjectType<O> objectType,
			final IFeature<? extends Number> feature, final TiconeClusteringResult clusteringResult) {
		super(new GridLayout());
		this.feature = feature;
		this.objectType = objectType;
		this.clusteringResult = clusteringResult;
		this.clusteringResult.addNewFeatureStoreListener(this);

		this.updateChartPanel();
	}

	private void updateChartPanel() {
		this.removeAll();

		final List<Integer> iterations = this.clusteringResult.getIterations();

		final XYSeriesCollection dataSet = new XYSeriesCollection();

		final XYSeries series = new XYSeries(this.feature.getName());
		dataSet.addSeries(series);

		for (int iteration : iterations) {
			final IFeatureStore featureStore = clusteringResult.getFeatureStore(iteration);
			final Set<O> objects = featureStore.keySet(objectType);
			for (O object : objects)
				series.add(iteration, featureStore.getFeatureValue(object, this.feature).getValue());
		}

		final JFreeChart jfreeChart = ChartFactory.createScatterPlot("", "Iteration", this.feature.getName(), dataSet,
				PlotOrientation.VERTICAL, // Orientation
				false, // Legend
				false, // Tooltips
				false // Urls
		);

		final XYPlot plot = jfreeChart.getXYPlot();
		plot.setInsets(RectangleInsets.ZERO_INSETS);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
		plot.setBackgroundPaint(Color.white);
		plot.setRangeGridlinePaint(Color.black);
		plot.getDomainAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());

		final XYItemRenderer renderer = plot.getRenderer();
		renderer.setSeriesPaint(0, Color.decode("#999999"));
		renderer.setSeriesPaint(1, Color.decode("#E69F00"));

		final ChartPanel chartPanel = new ChartPanel(jfreeChart);
		chartPanel.setMaximumDrawWidth(1920);
		chartPanel.setMaximumDrawHeight(1080);
		chartPanel.setDomainZoomable(true);
		chartPanel.setRangeZoomable(true);
		this.add(chartPanel);
		this.invalidate();
	}

	@Override
	public void newFeatureStore(NewFeatureStoreEvent e) {
		if (e.getClustering() == this.clusteringResult)
			e.getNewStore().addFeatureValueStoredListener(this);
	}

	@Override
	public void featureValuesStored(FeatureValuesStoredEvent e) {
		if (e.getObjectFeaturePairs().containsKey(this.feature))// && this.objectType.isInstance(e.getObject()))
			this.updateChartPanel();
	}
}
