/**
 * 
 */
package dk.sdu.imada.ticone.gui.panels.valuesample;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Optional;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.SeriesRenderingOrder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.util.Iterables;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 27, 2019
 *
 */
public class FeatureValueHistogramChartPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -983432219384306035L;

	protected final IFeature<?> feature;
	final protected Optional<List<IFeatureValue<? extends Comparable<?>>>> sample;
	final protected Optional<Iterable<IFeatureValue<? extends Comparable<?>>>> originalValues;
	private final JPanel controlPanel;

	private ChartPanel chartPanel;
	private SpinnerNumberModel numberBinsSpinnerModel;
	// only added to panel if both samples are provided
	private JCheckBox paintOriginalValuesCheckBox;

	public FeatureValueHistogramChartPanel(final IFeature<?> feature,
			final Optional<List<IFeatureValue<? extends Comparable<?>>>> sample,
			final Optional<Iterable<IFeatureValue<? extends Comparable<?>>>> originalValues)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException,
			NotAnArithmeticFeatureValueException, ToNumberConversionException {
		super(new BorderLayout());

		this.feature = feature;
		if (!sample.isPresent() && !originalValues.isPresent())
			throw new IllegalArgumentException(
					"At least either one of permuted or original values need to be specified");

		if ((sample.isPresent() && sample.get().isEmpty())
				|| (originalValues.isPresent() && !originalValues.get().iterator().hasNext()))
			throw new IllegalArgumentException("All feature value lists need to be non-empty");

		this.sample = sample;
		this.originalValues = originalValues;

		this.controlPanel = new JPanel(new FlowLayout());
		this.controlPanel.add(new JLabel("Number bins:"));
		this.numberBinsSpinnerModel = new SpinnerNumberModel(100, 1, 10000, 1);
		this.numberBinsSpinnerModel.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				if (chartPanel != null) {
					remove(chartPanel);
					try {
						chartPanel = getChartPanel();
						add(chartPanel, BorderLayout.CENTER);
					} catch (IncompatiblePrototypeComponentException | MissingPrototypeException | InterruptedException
							| NotAnArithmeticFeatureValueException | ToNumberConversionException e1) {
						e1.printStackTrace();
					}
					revalidate();
					repaint();
				}
			}
		});
		this.controlPanel.add(new JSpinner(numberBinsSpinnerModel));

		this.paintOriginalValuesCheckBox = new JCheckBox();
		this.paintOriginalValuesCheckBox.setSelected(true);
		this.paintOriginalValuesCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (chartPanel != null) {
					remove(chartPanel);
					try {
						chartPanel = getChartPanel();
						add(chartPanel, BorderLayout.CENTER);
					} catch (IncompatiblePrototypeComponentException | MissingPrototypeException | InterruptedException
							| NotAnArithmeticFeatureValueException | ToNumberConversionException e1) {
						e1.printStackTrace();
					}
					revalidate();
					repaint();
				}
			}
		});

		if (this.sample.isPresent() && this.originalValues.isPresent()) {
			this.controlPanel.add(new JLabel("Show original values:"));
			this.controlPanel.add(paintOriginalValuesCheckBox);
		}

		this.add(controlPanel, BorderLayout.NORTH);

		this.chartPanel = getChartPanel();
		this.add(chartPanel, BorderLayout.CENTER);
	}

	private ChartPanel getChartPanel() throws IncompatiblePrototypeComponentException, MissingPrototypeException,
			InterruptedException, NotAnArithmeticFeatureValueException, ToNumberConversionException {
		return this.getChartPanel(250, 100);
	}

	private ChartPanel getChartPanel(final int panelWidth, final int panelHeight)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException,
			NotAnArithmeticFeatureValueException, ToNumberConversionException {
		return this.getChartPanel(panelWidth, panelHeight, false);
	}

	private ChartPanel getChartPanel(final int preferredPanelWidth, final int prefferedPanelHeight,
			final boolean hideAxes) throws IncompatiblePrototypeComponentException, MissingPrototypeException,
			InterruptedException, NotAnArithmeticFeatureValueException, ToNumberConversionException {
		final HistogramDataset dataSet = new HistogramDataset();
		dataSet.setType(HistogramType.RELATIVE_FREQUENCY);
		double minValue = Double.POSITIVE_INFINITY, maxValue = Double.NEGATIVE_INFINITY;
		int numberBins = (Integer) this.numberBinsSpinnerModel.getValue();
		double[] permutedVals = null;
		if (this.sample.isPresent()) {
			permutedVals = new double[sample.get().size()];
			int i = 0;
			for (IFeatureValue<?> fv : sample.get()) {
				double v = fv.toNumber().doubleValue();
				permutedVals[i++] = v;
				if (v < minValue)
					minValue = v;
				if (v > maxValue)
					maxValue = v;
			}
		}
		final JFreeChart jfreeChart = ChartFactory.createHistogram(
//				String.format("Distribution of '%s'", feature.getName()), // Chart
				"",
				// label/name
				feature.getName(), // X-axis label
				"Relative Frequency", // Y-axis label
				dataSet, // Dataset
				PlotOrientation.VERTICAL, // Orientation
				true, // Legend
				true, // Tooltips
				false // Urls
		);
		final XYPlot plot = jfreeChart.getXYPlot();
		plot.setInsets(RectangleInsets.ZERO_INSETS);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
		plot.setBackgroundPaint(Color.white);
		plot.setRangeGridlinePaint(Color.black);
		plot.setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);

		final XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
		renderer.setSeriesPaint(0, Color.decode("#999999"));
		renderer.setSeriesPaint(1, Color.decode("#E69F00"));
		// prevent gradients
		renderer.setBarPainter(new StandardXYBarPainter());

		double[] originalVals = null;
		if (this.originalValues.isPresent() && this.paintOriginalValuesCheckBox.isSelected()) {
			originalVals = Iterables.toList(this.originalValues.get()).stream().mapToDouble(v -> {
				try {
					return v.toNumber().doubleValue();
				} catch (NotAnArithmeticFeatureValueException | ToNumberConversionException e) {
					return Double.NaN;
				}
			}).toArray();
			for (double v : originalVals) {
				if (v < minValue)
					minValue = v;
				if (v > maxValue)
					maxValue = v;
			}
		}

		if (permutedVals != null)
			dataSet.addSeries("Permuted", permutedVals, numberBins, minValue, maxValue);
		if (originalVals != null)
			dataSet.addSeries("Original", originalVals, numberBins, minValue, maxValue);

		// add transparency if both series are set
		if (permutedVals != null && originalVals != null)
			plot.setForegroundAlpha(0.85f);

		final ChartPanel chartPanel = new ChartPanel(jfreeChart, preferredPanelWidth, prefferedPanelHeight,
				ChartPanel.DEFAULT_MINIMUM_DRAW_WIDTH, ChartPanel.DEFAULT_MINIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_MAXIMUM_DRAW_WIDTH, ChartPanel.DEFAULT_MAXIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_BUFFER_USED, true, true, true, true, true);
		chartPanel.setDomainZoomable(true);
		chartPanel.setRangeZoomable(true);

		chartPanel.setMinimumSize(new Dimension(preferredPanelWidth, prefferedPanelHeight));
		return chartPanel;
	}

}
