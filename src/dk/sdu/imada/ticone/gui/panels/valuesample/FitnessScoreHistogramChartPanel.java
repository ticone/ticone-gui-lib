/**
 * 
 */
package dk.sdu.imada.ticone.gui.panels.valuesample;

import java.awt.Color;
import java.awt.Dimension;
import java.io.Serializable;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;

import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 28, 2019
 *
 */
public class FitnessScoreHistogramChartPanel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2757323360294252641L;

	final protected IFitnessScore fitnessScore;

	final protected List<Double> sample;

	public FitnessScoreHistogramChartPanel(final IFitnessScore fitnessScore, final List<Double> sample)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
		super();
		this.fitnessScore = fitnessScore;
		this.sample = sample;

	}

	public ChartPanel getChartPanel()
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		return this.getChartPanel(250, 100);
	}

	public ChartPanel getChartPanel(final int panelWidth, final int panelHeight)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		return this.getChartPanel(panelWidth, panelHeight, false);
	}

	public ChartPanel getChartPanel(final int preferredPanelWidth, final int prefferedPanelHeight,
			final boolean hideAxes)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException {
		final HistogramDataset dataSet = new HistogramDataset();
		dataSet.setType(HistogramType.RELATIVE_FREQUENCY);
		dataSet.addSeries("1", sample.stream().mapToDouble(d -> d.doubleValue()).toArray(), 1000);
		final JFreeChart jfreeChart = ChartFactory.createHistogram(
				String.format("Distribution of '%s' for random objects", fitnessScore.getName()), // Chart
				// label/name
				"", // X-axis label
				"", // Y-axis label
				dataSet, // Dataset
				PlotOrientation.VERTICAL, // Orientation
				false, // Legend
				false, // Tooltips
				false // Urls
		);
		final XYPlot plot = jfreeChart.getXYPlot();
		plot.setInsets(RectangleInsets.ZERO_INSETS);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
		plot.setBackgroundPaint(Color.white);
		plot.setRangeGridlinePaint(Color.black);

		final XYItemRenderer renderer = plot.getRenderer();
		renderer.setSeriesPaint(0, Color.decode("#999999"));
		renderer.setSeriesPaint(1, Color.decode("#E69F00"));

		final ChartPanel chartPanel = new ChartPanel(jfreeChart, preferredPanelWidth, prefferedPanelHeight,
				ChartPanel.DEFAULT_MINIMUM_DRAW_WIDTH, ChartPanel.DEFAULT_MINIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_MAXIMUM_DRAW_WIDTH, ChartPanel.DEFAULT_MAXIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_BUFFER_USED, true, true, true, true, true);
		chartPanel.setDomainZoomable(true);
		chartPanel.setRangeZoomable(true);

		chartPanel.setMinimumSize(new Dimension(preferredPanelWidth, prefferedPanelHeight));
		return chartPanel;
	}

}
