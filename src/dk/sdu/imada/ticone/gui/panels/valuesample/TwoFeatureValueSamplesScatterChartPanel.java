/**
 * 
 */
package dk.sdu.imada.ticone.gui.panels.valuesample;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.SeriesRenderingOrder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 31, 2019
 *
 */
public class TwoFeatureValueSamplesScatterChartPanel<O extends IObjectWithFeatures> extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 633043359637603108L;

	private final List<IFeatureValue<? extends Comparable<?>>> sample1, sample2;
	private final Optional<List<IFeatureValue<? extends Comparable<?>>>> originalValues1, originalValues2;

	private final JPanel controlPanel;
	private ChartPanel chartPanel;

	// only added to panel if both samples are provided
	private JCheckBox paintOriginalValuesCheckBox;

	public TwoFeatureValueSamplesScatterChartPanel(final List<IFeatureValue<? extends Comparable<?>>> sample1,
			final List<IFeatureValue<? extends Comparable<?>>> sample2,
			final Optional<List<IFeatureValue<? extends Comparable<?>>>> originalValues1,
			final Optional<List<IFeatureValue<? extends Comparable<?>>>> originalValues2)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException,
			NotAnArithmeticFeatureValueException, ToNumberConversionException {
		super(new BorderLayout());

		if (sample1.isEmpty() || sample1.size() != sample2.size()) {
			throw new IllegalArgumentException("All value lists need to have the same length and need to be non-empty");
		}

		this.sample1 = sample1;
		this.sample2 = sample2;
		this.originalValues1 = originalValues1;
		this.originalValues2 = originalValues2;

		this.controlPanel = new JPanel(new FlowLayout());
		this.controlPanel.add(new JLabel("Number bins:"));

		this.paintOriginalValuesCheckBox = new JCheckBox();
		this.paintOriginalValuesCheckBox.setSelected(true);
		this.paintOriginalValuesCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (chartPanel != null) {
					remove(chartPanel);
					try {
						chartPanel = getChartPanel();
						add(chartPanel, BorderLayout.CENTER);
					} catch (IncompatiblePrototypeComponentException | MissingPrototypeException | InterruptedException
							| NotAnArithmeticFeatureValueException | ToNumberConversionException e1) {
						e1.printStackTrace();
					}
					revalidate();
					repaint();
				}
			}
		});

		if (this.originalValues1.isPresent() && this.originalValues2.isPresent()) {
			this.controlPanel.add(new JLabel("Show original values:"));
			this.controlPanel.add(paintOriginalValuesCheckBox);
		}

		this.add(controlPanel, BorderLayout.NORTH);

		this.chartPanel = getChartPanel();
		this.add(chartPanel, BorderLayout.CENTER);

	}

	private ChartPanel getChartPanel() throws IncompatiblePrototypeComponentException, MissingPrototypeException,
			InterruptedException, NotAnArithmeticFeatureValueException, ToNumberConversionException {
		return this.getChartPanel(250, 100);
	}

	private ChartPanel getChartPanel(final int panelWidth, final int panelHeight)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException, InterruptedException,
			NotAnArithmeticFeatureValueException, ToNumberConversionException {
		return this.getChartPanel(panelWidth, panelHeight, false);
	}

	private ChartPanel getChartPanel(final int preferredPanelWidth, final int prefferedPanelHeight,
			final boolean hideAxes) throws IncompatiblePrototypeComponentException, MissingPrototypeException,
			InterruptedException, NotAnArithmeticFeatureValueException, ToNumberConversionException {

		final IFeature<?> f1 = this.sample1.get(0).getFeature();
		final IFeature<?> f2 = this.sample2.get(0).getFeature();

		final XYSeries seriesPermuted = new XYSeries("Permuted", false);
		for (int i = 0; i < this.sample1.size(); i++) {
			seriesPermuted.add(sample1.get(i).toNumber().doubleValue(), sample2.get(i).toNumber().doubleValue());
		}

		final XYSeriesCollection dataSet = new XYSeriesCollection();
		dataSet.addSeries(seriesPermuted);

		final JFreeChart jfreeChart = ChartFactory.createScatterPlot("", f1.getName(), f2.getName(), dataSet,
				PlotOrientation.VERTICAL, // Orientation
				true, // Legend
				true, // Tooltips
				false // Urls
		);
		final XYPlot plot = jfreeChart.getXYPlot();
		plot.setInsets(RectangleInsets.ZERO_INSETS);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
		plot.setBackgroundPaint(Color.white);
		plot.setRangeGridlinePaint(Color.black);
		plot.setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);

		final XYItemRenderer renderer = plot.getRenderer();
		renderer.setSeriesPaint(0, Color.decode("#999999"));

		if (this.originalValues1.isPresent() && this.originalValues2.isPresent()
				&& this.paintOriginalValuesCheckBox.isSelected()) {
			final XYSeries seriesOriginal = new XYSeries("Original", false);
			dataSet.addSeries(seriesOriginal);
			renderer.setSeriesPaint(1, Color.decode("#E69F00"));

			final Iterator<IFeatureValue<? extends Comparable<?>>> it1 = this.originalValues1.get().iterator(),
					it2 = this.originalValues2.get().iterator();

			while (it1.hasNext() && it2.hasNext()) {
				final IFeatureValue<? extends Comparable<?>> v1 = it1.next(), v2 = it2.next();
				assert (Objects.equals(v1.getObject(), v2.getObject()));
				seriesOriginal.add(v1.toNumber().doubleValue(), v2.toNumber().doubleValue());
			}
		}

		final StandardXYToolTipGenerator tooltipGenerator = new StandardXYToolTipGenerator() {

			@Override
			public String generateLabelString(XYDataset dataset, int series, int item) {
				return super.generateLabelString(dataset, series, item);
			}

			@Override
			public String generateToolTip(XYDataset dataset, int series, int item) {
				if (series == 1 && originalValues1.isPresent() && originalValues1.get().get(item).getObject() != null)
					return String.format("%s: %s", originalValues1.get().get(item).getObject(),
							super.generateToolTip(dataset, series, item));

				return super.generateToolTip(dataset, series, item);
			}
		};
		renderer.setDefaultToolTipGenerator(tooltipGenerator);

		final ChartPanel chartPanel = new ChartPanel(jfreeChart, preferredPanelWidth, prefferedPanelHeight,
				ChartPanel.DEFAULT_MINIMUM_DRAW_WIDTH, ChartPanel.DEFAULT_MINIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_MAXIMUM_DRAW_WIDTH, ChartPanel.DEFAULT_MAXIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_BUFFER_USED, true, true, true, true, true);
		chartPanel.setDomainZoomable(true);
		chartPanel.setRangeZoomable(true);

		chartPanel.setMinimumSize(new Dimension(preferredPanelWidth, prefferedPanelHeight));
		return chartPanel;
	}

}
