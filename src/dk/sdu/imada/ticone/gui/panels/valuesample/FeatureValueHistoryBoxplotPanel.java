/**
 * 
 */
package dk.sdu.imada.ticone.gui.panels.valuesample;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.BoxAndWhiskerToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BoxAndWhiskerRenderer;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;

import dk.sdu.imada.ticone.clustering.TiconeClusteringResult;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.FeatureValuesStoredEvent;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.feature.store.IFeatureValueStoredListener;
import dk.sdu.imada.ticone.feature.store.INewFeatureStoreListener;
import dk.sdu.imada.ticone.feature.store.NewFeatureStoreEvent;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 4, 2019
 *
 * @param <O>
 */
public class FeatureValueHistoryBoxplotPanel<O extends IObjectWithFeatures> extends JPanel
		implements INewFeatureStoreListener, IFeatureValueStoredListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7434885749735593447L;
	protected final ObjectType<O> objectType;
	protected final IFeature<? extends Number> feature;
	protected final TiconeClusteringResult clusteringResult;

	public FeatureValueHistoryBoxplotPanel(final ObjectType<O> objectType, final IFeature<? extends Number> feature,
			final TiconeClusteringResult clusteringResult) {
		super(new GridLayout());
		this.objectType = objectType;
		this.feature = feature;
		this.clusteringResult = clusteringResult;
		this.clusteringResult.addNewFeatureStoreListener(this);
		this.clusteringResult.getFeatureStore().addFeatureValueStoredListener(this);

		updateChartPanel();
	}

	private void updateChartPanel() {
		this.removeAll();
		final List<Integer> iterations = this.clusteringResult.getIterations();

		final DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();

		for (int iteration : iterations) {
			final IFeatureStore featureStore = this.clusteringResult.getFeatureStore(iteration);

			final List<Double> values = featureStore.keySet(objectType).stream()
					.map(o -> featureStore.getFeatureValue(o, feature).getValue().doubleValue())
					.collect(Collectors.toCollection(() -> new ArrayList<>()));
			dataset.add(values, feature.getName(), iteration);
		}

		final CategoryAxis xAxis = new CategoryAxis("Iteration");
		final NumberAxis yAxis = new NumberAxis(feature.getName());
		yAxis.setAutoRangeIncludesZero(false);
		final BoxAndWhiskerRenderer renderer = new BoxAndWhiskerRenderer();
		renderer.setFillBox(false);
		renderer.setDefaultToolTipGenerator(new BoxAndWhiskerToolTipGenerator());
		final CategoryPlot plot = new CategoryPlot(dataset, xAxis, yAxis, renderer);

		plot.setInsets(RectangleInsets.ZERO_INSETS);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
		plot.setBackgroundPaint(Color.white);
		plot.setRangeGridlinePaint(Color.black);

		final JFreeChart chart = new JFreeChart("", new Font("SansSerif", Font.BOLD, 14), plot, true);
		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setMaximumDrawWidth(1920);
		chartPanel.setMaximumDrawHeight(1080);
		chartPanel.setPreferredSize(new java.awt.Dimension(450, 270));
		this.add(chartPanel);
		this.invalidate();
	}

	@Override
	public void newFeatureStore(NewFeatureStoreEvent e) {
		if (e.getClustering() == this.clusteringResult) {
			e.getNewStore().addFeatureValueStoredListener(this);
		}
	}

	@Override
	public void featureValuesStored(FeatureValuesStoredEvent e) {
		if (e.getObjectFeaturePairs().containsKey(this.feature))// && this.objectType.isInstance(e.getObject()))
			this.updateChartPanel();
	}
}
