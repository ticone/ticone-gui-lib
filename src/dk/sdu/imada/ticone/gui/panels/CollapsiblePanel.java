package dk.sdu.imada.ticone.gui.panels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FontMetrics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.ToolTipManager;
import javax.swing.border.TitledBorder;

public class CollapsiblePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2118425036191622913L;
	protected String title;
	protected TitledBorder border;
	protected boolean currentVisibility, isArrowHover;

	public CollapsiblePanel(final String title, final boolean defaultVisibility) {
		super();
		this.title = title;
		this.currentVisibility = defaultVisibility;
		this.createBorder();
		this.setBorder(this.border);
		final BorderLayout borderLayout = new BorderLayout();
		this.setLayout(borderLayout);
		ToolTipManager.sharedInstance().registerComponent(this);
		this.contentComponentListener = new ComponentAdapter() {
			@Override
			public void componentShown(final ComponentEvent e) {
				CollapsiblePanel.this.updateBorderTitle();
			}

			@Override
			public void componentHidden(final ComponentEvent e) {
				CollapsiblePanel.this.updateBorderTitle();
			}
		};
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					if (isOverArrowOrTitle(e.getX(), e.getY()))
						CollapsiblePanel.this.toggleVisibility();
				}
				super.mouseClicked(e);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setIsArrowHover(false);
				super.mouseExited(e);
			}
		});
		this.addMouseMotionListener(new MouseAdapter() {

			@Override
			public void mouseMoved(MouseEvent e) {
				setIsArrowHover(isOverArrowOrTitle(e.getX(), e.getY()));
				super.mouseMoved(e);
			}
		});
	}

	protected void createBorder() {
		if (this.border == null)
			this.border = BorderFactory.createTitledBorder(this.title);

		if (!this.currentVisibility)
			this.border.setBorder(BorderFactory.createEmptyBorder());
		else
			this.border.setBorder(BorderFactory.createTitledBorder("test").getBorder());
		this.border.setTitlePosition(TitledBorder.TOP);
		this.border.setTitleJustification(TitledBorder.LEFT);
	}

	private boolean isOverArrowOrTitle(int x, int y) {
		final FontMetrics fontMetrics = this.getFontMetrics(this.getFont());
		int fontTextHeight = fontMetrics.getHeight();
		int arrowWidth = fontMetrics.stringWidth(String.format("▽ %s", this.title));
		int baseBorderOffSetX = this.getInsets().left + 3;

		return x >= baseBorderOffSetX && x <= baseBorderOffSetX + arrowWidth && y <= fontTextHeight;
	}

	final private ComponentListener contentComponentListener;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.firePropertyChange("title", this.title, this.title = title);
	}

	@Override
	public String getToolTipText(MouseEvent event) {
		if (isOverArrowOrTitle(event.getX(), event.getY()))
			return currentVisibility ? "Collapse panel" : "Open panel";
		return super.getToolTipText(event);
	}

	private void setIsArrowHover(final boolean isArrowHover) {
		boolean wasHover = this.isArrowHover;
		this.isArrowHover = isArrowHover;
		if (isArrowHover)
			setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		else
			setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		if (wasHover != isArrowHover)
			updateBorderTitle();
	}

	private Component wrapInPanel(final Component comp) {
		final JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(comp, BorderLayout.CENTER);
		return panel;
	}

	@Override
	public Component add(Component comp) {
		comp = this.wrapInPanel(comp);
		comp.addComponentListener(this.contentComponentListener);
		final Component r = super.add(comp);
		r.setVisible(this.currentVisibility);
		this.updateBorderTitle();
		return r;
	}

	@Override
	public Component add(final String name, Component comp) {
		comp = this.wrapInPanel(comp);
		comp.addComponentListener(this.contentComponentListener);
		final Component r = super.add(name, comp);
		r.setVisible(this.currentVisibility);
		this.updateBorderTitle();
		return r;
	}

	@Override
	public Component add(Component comp, final int index) {
		comp = this.wrapInPanel(comp);
		comp.addComponentListener(this.contentComponentListener);
		final Component r = super.add(comp, index);
		r.setVisible(this.currentVisibility);
		this.updateBorderTitle();
		return r;
	}

	@Override
	public void add(Component comp, final Object constraints) {
		comp = this.wrapInPanel(comp);
		comp.addComponentListener(this.contentComponentListener);
		super.add(comp, constraints);
		comp.setVisible(this.currentVisibility);
		this.updateBorderTitle();
	}

	@Override
	public void add(Component comp, final Object constraints, final int index) {
		comp = this.wrapInPanel(comp);
		comp.addComponentListener(this.contentComponentListener);
		super.add(comp, constraints, index);
		comp.setVisible(this.currentVisibility);
		this.updateBorderTitle();
	}

	@Override
	public void remove(final int index) {
		final Component comp = this.getComponent(index);
		comp.removeComponentListener(this.contentComponentListener);
		super.remove(index);
	}

	@Override
	public void remove(final Component comp) {
		comp.removeComponentListener(this.contentComponentListener);
		super.remove(comp);
	}

	@Override
	public void removeAll() {
		for (final Component c : this.getComponents()) {
			c.removeComponentListener(this.contentComponentListener);
		}
		super.removeAll();
	}

	protected void toggleVisibility() {
		this.toggleVisibility(!this.currentVisibility);
	}

	protected void toggleVisibility(final boolean visible) {
		for (final Component c : this.getComponents()) {
			c.setVisible(visible);
		}
		this.currentVisibility = visible;
		this.createBorder();
		this.updateBorderTitle();
	}

	protected void updateBorderTitle() {
		String arrow = "";
		if (this.getComponentCount() > 0) {
			if (this.isArrowHover)
				arrow = (this.currentVisibility ? "▽" : "▷");
			else
				arrow = (this.currentVisibility ? "▼" : "▶");
		}
		this.border.setTitle(arrow + " " + this.title);
		this.repaint();
	}

	// protected final boolean hasInvisibleComponent() {
	// for (Component c : getComponents()) {
	// if (!c.isVisible()) {
	// return true;
	// }
	// }
	// return false;
	// }

}