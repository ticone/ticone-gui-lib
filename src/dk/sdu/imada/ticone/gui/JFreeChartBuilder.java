/**
 * 
 */
package dk.sdu.imada.ticone.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer.CHART_Y_LIMITS_TYPE;
import dk.sdu.imada.ticone.prototype.AbstractBuilder;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.Pair;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 26, 2018
 *
 */
public class JFreeChartBuilder extends AbstractBuilder<JFreeChart, FactoryException, CreateInstanceFactoryException> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -560064867892389715L;

	protected transient Map<Pair<Long, ITimeSeriesObjects>, Map<Pair<Boolean, CHART_Y_LIMITS_TYPE>, JFreeChart>> dimensionsToCharts = new HashMap<>();

	protected transient Map<Pair<Long, ITimeSeriesObjects>, Map<Pair<Boolean, CHART_Y_LIMITS_TYPE>, JFreeChart>> dimensionsToLargeCharts = new HashMap<>();

	private transient boolean largeChart;

	private transient ICluster cluster;

	private transient XYSeriesCollection dataset;

	private transient ITimeSeriesObjects allObjects;

	private transient int numberVisualizedObjects;

	private transient Map<ITimeSeriesObject, Integer> objectsDatasetIndex;

	private transient int timePoints;

	private transient Double[][] percentiles;

	private transient List<List<Double>> allTimePoints;

	private transient Color graphColor;

	private CHART_Y_LIMITS_TYPE limitsType;

	private transient XYSeriesCollection largeDataset; // For the large chart

	private transient boolean hideAxes;

	private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		this.dimensionsToCharts = new HashMap<>();
		this.dimensionsToLargeCharts = new HashMap<>();
	}

	/**
	 * @param limitsType the limitsType to set
	 */
	public void setLimitsType(final CHART_Y_LIMITS_TYPE limitsType) {
		this.limitsType = limitsType;
	}

	/**
	 * @return the limitsType
	 */
	public CHART_Y_LIMITS_TYPE getLimitsType() {
		return this.limitsType;
	}

	/**
	 * @param largeChart the largeChart to set
	 */
	public void setLargeChart(final boolean largeChart) {
		this.largeChart = largeChart;
	}

	/**
	 * @return the largeChart
	 */
	public boolean isLargeChart() {
		return this.largeChart;
	}

	public ICluster getCluster() {
		return this.cluster;
	}

	public void setCluster(final ICluster cluster) {
		this.cluster = cluster;
	}

	public ITimeSeriesObjects getAllObjects() {
		return this.allObjects;
	}

	public void setAllObjects(final ITimeSeriesObjects allObjects2) {
		this.allObjects = allObjects2;
		this.dataset = null;
		this.largeDataset = null;
	}

	public int getNumberVisualizedObjects() {
		return this.numberVisualizedObjects;
	}

	public void setNumberVisualizedObjects(final int numberVisualizedObjects) {
		this.numberVisualizedObjects = numberVisualizedObjects;
	}

	public Map<ITimeSeriesObject, Integer> getObjectsDatasetIndex() {
		return this.objectsDatasetIndex;
	}

	public void setObjectsDatasetIndex(final Map<ITimeSeriesObject, Integer> objectsDatasetIndex) {
		this.objectsDatasetIndex = objectsDatasetIndex;
	}

	public int getTimePoints() {
		return this.timePoints;
	}

	public void setTimePoints(final int timePoints) {
		this.timePoints = timePoints;
	}

	/**
	 * @param hideAxes the hideAxes to set
	 */
	public void setHideAxes(boolean hideAxes) {
		this.hideAxes = hideAxes;
	}

	/**
	 * @return the hideAxes
	 */
	public boolean isHideAxes() {
		return this.hideAxes;
	}

	public Double[][] getPercentiles() {
		return this.percentiles;
	}

	public void setPercentiles(final Double[][] percentiles) {
		this.percentiles = percentiles;
	}

	public List<List<Double>> getAllTimePoints() {
		return this.allTimePoints;
	}

	public void setAllTimePoints(final List<List<Double>> allTimePoints) {
		this.allTimePoints = allTimePoints;
	}

	public Color getGraphColor() {
		return this.graphColor;
	}

	public void setGraphColor(final Color graphColor) {
		this.graphColor = graphColor;
	}

//	public void setDataset(XYSeriesCollection dataset)
//			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
//		this.dataset = dataset;
//		setupDataset(dataset);
//	}

	@Override
	public JFreeChartBuilder copy() {
		final JFreeChartBuilder result = new JFreeChartBuilder();
		result.allObjects = this.allObjects;
		result.allTimePoints = this.allTimePoints;
		result.cluster = this.cluster;
		result.dataset = this.dataset;
		result.dimensionsToCharts = this.dimensionsToCharts;
		result.dimensionsToLargeCharts = this.dimensionsToLargeCharts;
		result.graphColor = this.graphColor;
		result.largeChart = this.largeChart;
		result.largeDataset = this.largeDataset;
		result.limitsType = this.limitsType;
		result.numberVisualizedObjects = this.numberVisualizedObjects;
		result.objectsDatasetIndex = this.objectsDatasetIndex;
		result.percentiles = this.percentiles;
		result.timePoints = this.timePoints;
		return result;
	}

	@Override
	protected JFreeChart doBuild() throws FactoryException, CreateInstanceFactoryException {
		try {
			final Pair<Long, ITimeSeriesObjects> clusterIdAndObjects = Pair.of(
					this.cluster != null ? Long.valueOf(this.cluster.getInternalClusterId()) : null, this.allObjects);

			final Pair<Boolean, CHART_Y_LIMITS_TYPE> pair = Pair.getPair(this.hideAxes, this.limitsType);

			if (this.largeChart) {
				if (!this.dimensionsToLargeCharts.containsKey(clusterIdAndObjects)
						|| !this.dimensionsToLargeCharts.get(clusterIdAndObjects).containsKey(pair)) {
					final JFreeChart largeJFreeChart = this.setupXYLineChart(this.getLargeDataset());
					largeJFreeChart.setNotify(false);

					final ChartPanel chartPanel = new ChartPanel(largeJFreeChart);
					chartPanel.setDomainZoomable(false);
					chartPanel.setRangeZoomable(true);

					chartPanel.setPreferredSize(new Dimension(750, 300));
					final XYPlot plot = largeJFreeChart.getXYPlot();

					// Set tick to go 1 up at a time
					this.setupNumberAxis(plot);

					// Set y-axis to go from -3 to 3
					this.setupValueAxis(plot);

					final XYLineAndShapeRenderer renderer = this.setupBasicRenderer();

					plot.setRenderer(renderer);
					largeJFreeChart.setNotify(true);
					this.dimensionsToLargeCharts.putIfAbsent(clusterIdAndObjects, new HashMap<>());
					this.dimensionsToLargeCharts.get(clusterIdAndObjects).put(pair, largeJFreeChart);
				}
				return this.dimensionsToLargeCharts.get(clusterIdAndObjects).get(pair);
			} else {
				if (!this.dimensionsToCharts.containsKey(clusterIdAndObjects)
						|| !this.dimensionsToCharts.get(clusterIdAndObjects).containsKey(pair)) {
					final JFreeChart jFreeChart = this.setupXYLineChart(this.getDataset());
					jFreeChart.setNotify(false);

					final XYPlot plot = jFreeChart.getXYPlot();

					// Set tick to go 1 up at a time
					this.setupNumberAxis(plot);

					// Set y-axis
					this.setupValueAxis(plot);

					final XYLineAndShapeRenderer renderer = this.setupBasicRenderer();

					plot.setRenderer(renderer);
					this.dimensionsToCharts.putIfAbsent(clusterIdAndObjects, new HashMap<>());
					this.dimensionsToCharts.get(clusterIdAndObjects).put(pair, jFreeChart);

					// writeChartAsPNG(jFreeChart);
				}
				return this.dimensionsToCharts.get(clusterIdAndObjects).get(pair);
			}
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			throw new CreateInstanceFactoryException(e);
		}
	}

	@Override
	public void reset() {
	}

	public XYSeriesCollection getDataset() throws IncompatiblePrototypeComponentException, MissingPrototypeException {
		if (this.dataset == null) {
			this.dataset = new XYSeriesCollection();
			this.setupDataset(this.dataset);
		}
		return this.dataset;
	}

	private void setupDataset(final XYSeriesCollection dataset)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
		int currentIndex = 0;
		if (this.cluster != null) {
			final XYSeries patternSeries = this.generateDataset(this.cluster.toString(), "",
					PrototypeComponentType.TIME_SERIES.getComponent(this.cluster.getPrototype()).getTimeSeries()
							.asArray());
			dataset.addSeries(patternSeries);
			currentIndex = 1;
		}

		double[][] vals = null;

		int currRow = 0;

		final List<XYSeries> series = new ArrayList<>();

		int numberReplicates = 0;
		if (this.numberVisualizedObjects > 0) {
			numberReplicates = this.allObjects.iterator().next().getPreprocessedTimeSeriesList().length;
		}

		vals = new double[this.timePoints][this.numberVisualizedObjects * numberReplicates];

		int oc = 0;
		for (final ITimeSeriesObject timeSeriesData : this.allObjects) {
			final ITimeSeries[] exactPatternList = timeSeriesData.getPreprocessedTimeSeriesList();

			this.objectsDatasetIndex.put(timeSeriesData, currentIndex);
			for (int j = 0; j < exactPatternList.length; j++) {
				final ITimeSeries exactPattern = exactPatternList[j];
				final String sample = timeSeriesData.getSampleNameList().get(j);
				final XYSeries xySeries = this.generateDataset(timeSeriesData.getName(), sample,
						exactPattern.asArray());
				// dataset.addSeries(xySeries);
				series.add(xySeries);
				currentIndex++;

				for (int t = 0; t < exactPattern.getNumberTimePoints(); t++)
					vals[t][currRow] = exactPattern.get(t);
				currRow++;
			}
			oc++;
			if (oc >= this.numberVisualizedObjects)
				break;
		}

		// add percentile lines
		for (int t = 0; t < vals.length; t++) {
			Arrays.sort(vals[t]);

			if (vals[t].length > 0) {
				this.percentiles[t][0] = vals[t][0];
				this.percentiles[t][1] = vals[t][Math.max((int) Math.round(vals[t].length * 0.05) - 1, 0)];
				this.percentiles[t][2] = vals[t][Math.max((int) Math.round(vals[t].length * 0.25) - 1, 0)];
				this.percentiles[t][3] = vals[t][Math.max((int) Math.round(vals[t].length * 0.75) - 1, 0)];
				this.percentiles[t][4] = vals[t][Math.max((int) Math.round(vals[t].length * 0.95) - 1, 0)];
				this.percentiles[t][5] = vals[t][vals[t].length - 1];
			}
		}

		final XYSeries minPointsSeries = new XYSeries("min points");
		final XYSeries maxPointsSeries = new XYSeries("max points");
		final XYSeries percent25PointsSeries = new XYSeries("25p points");
		final XYSeries percent75PointsSeries = new XYSeries("75p points");
		final XYSeries percent5PointsSeries = new XYSeries("5p points");
		final XYSeries percent95PointsSeries = new XYSeries("95p points");
		for (int i = 0; i < this.percentiles.length; i++) {
			if (this.percentiles[i].length > 0) {
				minPointsSeries.add(i + 1, this.percentiles[i][0]);
				percent5PointsSeries.add(i + 1, this.percentiles[i][1]);
				percent25PointsSeries.add(i + 1, this.percentiles[i][2]);
				percent75PointsSeries.add(i + 1, this.percentiles[i][3]);
				percent95PointsSeries.add(i + 1, this.percentiles[i][4]);
				maxPointsSeries.add(i + 1, this.percentiles[i][5]);
			}
		}
		dataset.addSeries(minPointsSeries);
		dataset.addSeries(maxPointsSeries);
		dataset.addSeries(percent25PointsSeries);
		dataset.addSeries(percent75PointsSeries);
		dataset.addSeries(percent5PointsSeries);
		dataset.addSeries(percent95PointsSeries);

		for (final XYSeries s : series)
			dataset.addSeries(s);
	}

	private JFreeChart setupXYLineChart(final XYSeriesCollection xySeriesCollection) {
		final JFreeChart xyLineChart = ChartFactory.createXYLineChart("", // Chart
				// label/name
				"", // X-axis label
				"", // Y-axis label
				xySeriesCollection, // Dataset
				PlotOrientation.VERTICAL, // Orientation
				false, // Legend
				false, // Tooltips
				false // Urls
		);
		final XYPlot plot = xyLineChart.getXYPlot();
		plot.setInsets(RectangleInsets.ZERO_INSETS);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
		plot.setBackgroundPaint(Color.white);
		plot.setRangeGridlinePaint(Color.black);
		return xyLineChart;
	}

	private XYSeries generateDataset(final String name, final String sample, final double[] data) {
		final XYSeries xySeries = new XYSeries(sample + ":" + name);
		if (data != null) {
			for (int i = 0; i < data.length; i++) {
				this.allTimePoints.get(i).add(data[i]);
				xySeries.add(i + 1, data[i]);
			}
		}
		return xySeries;
	}

	private void setupNumberAxis(final XYPlot plot) {
		final NumberAxis numberAxis = new NumberAxis();
		numberAxis.setTickUnit(new NumberTickUnit(1));
		numberAxis.setRange(1, Math.max(this.timePoints, 2));
		if (this.hideAxes)
			numberAxis.setVisible(false);
		plot.setDomainAxis(numberAxis);
	}

	private void setupValueAxis(final XYPlot plot) {
		// Minimum and maximum values on graphs/charts
		double min_value = Double.MAX_VALUE, max_value = -Double.MAX_VALUE;

		final ValueAxis valueAxis = plot.getRangeAxis();
		if (this.limitsType.equals(CHART_Y_LIMITS_TYPE.FIVE_QUANTILE)) {
			for (int t = 0; t < this.timePoints; t++) {
				if (this.percentiles[t][1] < min_value)
					min_value = this.percentiles[t][1];
				if (this.percentiles[t][4] > max_value)
					max_value = this.percentiles[t][4];
			}

			valueAxis.setRange(min_value, max_value);
		} else if (this.limitsType.equals(CHART_Y_LIMITS_TYPE.TWENTYFIVE_QUANTILE)) {
			for (int t = 0; t < this.timePoints; t++) {
				if (this.percentiles[t][2] < min_value)
					min_value = this.percentiles[t][2];
				if (this.percentiles[t][3] > max_value)
					max_value = this.percentiles[t][3];
			}

			valueAxis.setRange(min_value, max_value);
		}
		if (this.hideAxes)
			valueAxis.setVisible(false);
		plot.setRangeAxis(valueAxis);
	}

	private XYLineAndShapeRenderer setupBasicRenderer() {
		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		// Line color of actual pattern
		renderer.setSeriesPaint(0, Color.black);

		// Do not show points for pattern
		renderer.setSeriesShapesVisible(0, false);

		int startIndex = 0;
		// Line width of actual pattern
		if (this.cluster != null) {
			renderer.setSeriesStroke(0, new BasicStroke(3f));
			startIndex = 1;
		}

		final float[] dash = { 10.0f };

		int red, green, blue;
		final int delta = 15;
		final boolean dark = !(this.graphColor.getRed() < delta && this.graphColor.getGreen() < delta
				&& this.graphColor.getBlue() < delta);
		if (dark) {
			red = this.graphColor.getRed() >= delta ? this.graphColor.getRed() - delta : this.graphColor.getRed();
			green = this.graphColor.getGreen() >= delta ? this.graphColor.getGreen() - delta
					: this.graphColor.getGreen();
			blue = this.graphColor.getBlue() >= delta ? this.graphColor.getBlue() - delta : this.graphColor.getBlue();
		} else {
			red = this.graphColor.getRed() <= (255 - delta) ? this.graphColor.getRed() + delta
					: this.graphColor.getRed();
			green = this.graphColor.getGreen() <= (255 - delta) ? this.graphColor.getGreen() + delta
					: this.graphColor.getGreen();
			blue = this.graphColor.getBlue() <= (255 - delta) ? this.graphColor.getBlue() + delta
					: this.graphColor.getBlue();
		}
		final Color dashColor = new Color(red, green, blue, 255);

		for (int i = startIndex; i < startIndex + this.percentiles[0].length; i++) {
			renderer.setSeriesPaint(i, dashColor);
			renderer.setSeriesShapesVisible(i, false);
			renderer.setSeriesStroke(i,
					new BasicStroke(2f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
			// renderer.setSeriesStroke(i, new BasicStroke(3f));
		}

		// Set color, points and width of exact patterns
		if (this.allObjects.size() < 1) {
			return renderer;
		}
		// int chartSize = sets *
		// timeSeriesDatas.get(0).getExactPatternList().size() + startIndex;
		for (int i = startIndex + this.percentiles[0].length; i < this.numberVisualizedObjects
				* this.allObjects.iterator().next().getPreprocessedTimeSeriesList().length + startIndex
				+ this.percentiles[0].length; i++) {
			renderer.setSeriesPaint(i, this.graphColor);
			renderer.setSeriesShapesVisible(i, false);
			renderer.setSeriesStroke(i, new BasicStroke(3f));
		}
		return renderer;
	}
//
//	/**
//	 * @param largeDataset the largeDataset to set
//	 * @throws MissingPrototypeException
//	 * @throws IncompatiblePrototypeComponentException
//	 */
//	public void setLargeDataset(XYSeriesCollection largeDataset)
//			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
//		this.largeDataset = largeDataset;
//		setupDataset(largeDataset);
//	}

	public XYSeriesCollection getLargeDataset()
			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
		if (this.largeDataset == null) {
			this.largeDataset = new XYSeriesCollection();
			this.setupDataset(this.largeDataset);
		}
		return this.largeDataset;
	}
}
