package dk.sdu.imada.ticone.clustering;

import javax.swing.JLabel;

import dk.sdu.imada.ticone.util.ITiconeResultNameChangeListener;
import dk.sdu.imada.ticone.util.TiconeResultNameChangedEvent;

public class ClusteringWithIterationLabel extends JLabel implements ITiconeResultNameChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5040054157684623973L;

	protected final static String formatString = "%s (Iteration %d)";

	protected AbstractNamedTiconeResult[] clusterings;

	protected int[] iterations;

	protected static String getStringForClusterings(final AbstractNamedTiconeResult[] clusterings,
			final int[] iterations) {
		final StringBuilder sb = new StringBuilder();

		for (int c = 0; c < clusterings.length; c++) {
			final AbstractNamedTiconeResult cl = clusterings[c];
			final int iteration = iterations[c];

			sb.append(String.format(formatString, cl.getName(), iteration));
			if (c < clusterings.length - 1)
				sb.append(" & ");
		}

		return sb.toString();
	}

	public ClusteringWithIterationLabel(final AbstractNamedTiconeResult[] clusterings, final int[] iterations) {
		super(getStringForClusterings(clusterings, iterations));
		this.clusterings = clusterings;
		this.iterations = iterations;
		for (final AbstractNamedTiconeResult c : clusterings)
			c.addNameChangeListener(this);
	}

	@Override
	public void resultNameChanged(final TiconeResultNameChangedEvent event) {
		final String newTitle = event.getNewName();
		this.setText(getStringForClusterings(this.clusterings, this.iterations));
		this.invalidate();
	}
}
