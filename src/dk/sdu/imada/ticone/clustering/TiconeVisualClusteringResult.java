package dk.sdu.imada.ticone.clustering;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.gui.JFreeChartBuilder;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer;
import dk.sdu.imada.ticone.gui.panels.clusterchart.ClusterChartContainer.CHART_Y_LIMITS_TYPE;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.ITiconeResultNameChangeListener;
import dk.sdu.imada.ticone.util.ITimePointWeighting;
import dk.sdu.imada.ticone.util.TiconeResultNameChangedEvent;

public class TiconeVisualClusteringResult extends TiconeClusteringResult implements ITiconeResultNameChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -36184057281199372L;
	protected JFreeChartBuilder chartFactory;
	protected transient Map<ICluster, ClusterChartContainer> defaultClusterChartContainer;
	protected transient Map<Pair<ICluster, ITimeSeriesObjects>, ClusterChartContainer> clusterChartContainer;
	private CHART_Y_LIMITS_TYPE chartLimits;

	public static TiconeVisualClusteringResult getInstance(final TiconeClusteringResult clusteringResult) {
		if (clusteringResult instanceof TiconeVisualClusteringResult)
			return (TiconeVisualClusteringResult) clusteringResult;
		return new TiconeVisualClusteringResult(clusteringResult);
	}

	public TiconeVisualClusteringResult(long seed, ILoadDataMethod loadDataMethod,
			IInitialClusteringProvider<ClusterObjectMapping> initialClusteringProvider, int numberOfTimePoints,
			ITimePointWeighting timePointWeighting, IIdMapMethod idMapMethod,
			IPreprocessingSummary preprocessingSummary, ISimilarityFunction similarityFunction,
			IClusteringMethodBuilder<? extends IClusteringMethod<ClusterObjectMapping>> clusteringMethod,
			ITimeSeriesPreprocessor timeSeriesPreprocessor, IPrototypeBuilder prototypeBuilder) {
		super(seed, loadDataMethod, initialClusteringProvider, numberOfTimePoints, timePointWeighting, idMapMethod,
				preprocessingSummary, similarityFunction, clusteringMethod, timeSeriesPreprocessor, prototypeBuilder);

		this.chartFactory = new JFreeChartBuilder();
		this.chartLimits = CHART_Y_LIMITS_TYPE.MIN_MAX;
		this.initClusterChartContainerMap();
		this.initDefaultClusterChartContainerMap();
	}

	protected TiconeVisualClusteringResult(final TiconeVisualClusteringResult clusteringResult) {
		super(clusteringResult);
		this.chartFactory = clusteringResult.chartFactory;
		this.clusterChartContainer = clusteringResult.clusterChartContainer;
		this.chartLimits = clusteringResult.chartLimits;
	}

	protected TiconeVisualClusteringResult(final TiconeClusteringResult clusteringResult) {
		super(clusteringResult);
		this.chartFactory = new JFreeChartBuilder();
		this.chartLimits = CHART_Y_LIMITS_TYPE.MIN_MAX;
		if (!(clusteringResult instanceof TiconeVisualClusteringResult)
				&& clusteringResult.getClass().isAssignableFrom(TiconeVisualClusteringResult.class))
			clusteringResult.addNameChangeListener(this);
		this.initClusterChartContainerMap();
		this.initDefaultClusterChartContainerMap();
	}
//
//	public void initChartPngMap() {
//		this.chartPngFiles = new HashMap<ICluster, String>();
//	}

	public CHART_Y_LIMITS_TYPE getChartLimits() {
		if (this.chartLimits == null) {
			this.chartLimits = CHART_Y_LIMITS_TYPE.MIN_MAX;
		}
		return this.chartLimits;
	}

	/**
	 * @return the chartFactory
	 */
	public JFreeChartBuilder getChartFactory() {
		return this.chartFactory;
	}

	public void setChartLimits(final CHART_Y_LIMITS_TYPE chartLimits) {
		if (chartLimits == null)
			return;
		this.chartLimits = chartLimits;
		// TODO: necessary to inform all listeners?
		this.fireStateChanged();
	}

	public boolean hasClusterChartContainer(final ICluster cluster, final ITimeSeriesObjects objects) {
		if (this.clusterChartContainer == null)
			this.initClusterChartContainerMap();
		return this.clusterChartContainer.containsKey(Pair.of(cluster, objects));
	}

	public ClusterChartContainer addClusterChartContainer(final ICluster cluster, final ITimeSeriesObjects objects,
			final ClusterChartContainer container) {
		if (this.clusterChartContainer == null)
			this.initClusterChartContainerMap();
		return this.clusterChartContainer.put(Pair.of(cluster, objects), container);
	}

	public void setDefaultClusterChartContainer(final ICluster cluster,
			final ClusterChartContainer defaultClusterChartContainer) {
		if (this.defaultClusterChartContainer == null)
			this.initDefaultClusterChartContainerMap();
		this.defaultClusterChartContainer.put(cluster, defaultClusterChartContainer);
	}

	public ClusterChartContainer getClusterChartContainer(final ICluster cluster, final ITimeSeriesObjects objects) {
		if (this.clusterChartContainer == null)
			this.initClusterChartContainerMap();
		return this.clusterChartContainer.get(Pair.of(cluster, objects));
	}

	public ClusterChartContainer getDefaultClusterChartContainer(final ICluster cluster) {
		if (this.defaultClusterChartContainer == null)
			this.initDefaultClusterChartContainerMap();
		return this.defaultClusterChartContainer.get(cluster);
	}

	public void initClusterChartContainerMap() {
		this.clusterChartContainer = new HashMap<>();
	}

	public void initDefaultClusterChartContainerMap() {
		this.defaultClusterChartContainer = new HashMap<>();
	}

	@Override
	public void resultNameChanged(final TiconeResultNameChangedEvent event) {
		final String newTitle = event.getNewName();
		this.name = newTitle;
		this.fireNameChanged();
	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		s.defaultReadObject();
		this.initClusterChartContainerMap();
		this.initDefaultClusterChartContainerMap();
	}
}